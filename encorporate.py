#!/usr/bin/env python
# Encorporate styles and scripts into HTML
# HTML file must have "data-in" attribute in script of style definition before closing tag
# Vitaly Checkryzhev <13hakta@gmail.com>, 2018

import os, sys, re

if len(sys.argv) < 3:
	print("Usage: encorporate.py src dest [use_compressed]")
	print("Not enough arguments")
	sys.exit(1)

r_js = re.compile(r'<script.+type="text\/javascript" src="(.+)" data-in><\/script>', re.IGNORECASE)
r_css = re.compile(r'<link rel="stylesheet" type="text\/css" href="(.+)" data-in.+?>', re.IGNORECASE)

strFileName = sys.argv[1]
strFileNameRes = sys.argv[2]

doPutMininames = 0

# replace file names with .min
if len(sys.argv) > 3:
	doPutMininames = sys.argv[3]

folder = os.path.dirname(strFileName)

def replaceJS(match):
	filename = match.group(1)

	if doPutMininames:
		newfilename = filename.replace('.js', '.min.js')
		print("Rename script: " + filename + " to " + newfilename)
		filename = newfilename

	objFile = open(folder + '/' + filename)
	fileContent = objFile.read()
	objFile.close()

	return '<script type="text/javascript">' + fileContent + '</script>'

def replaceCSS(match):
	filename = match.group(1)

	if doPutMininames:
		newfilename = filename.replace('.css', '.min.css')
		print("Rename style: " + filename + " to " + newfilename)
		filename = newfilename

	objFile = open(folder + '/' + filename)
	fileContent = objFile.read()
	objFile.close()

	return '<style>' + fileContent + '</style>'

# Get file contents
objFile = open(strFileName)
strText = objFile.read()
objFile.close()

# Replace
strNewText = r_js.sub(replaceJS, strText)
strNewText = r_css.sub(replaceCSS, strNewText)

# Write
objFile = open(strFileNameRes, 'w')
objFile.write(strNewText)
objFile.close()
