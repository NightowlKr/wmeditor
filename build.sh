#!/bin/sh
# Build wmeditor package

# Prepare folders
mkdir -p wmeditor/css
mkdir -p wmeditor/js

# Copy scripts
cp src/index.php wmeditor
cp src/jsm.php wmeditor
cp src/setup.php wmeditor
cp src/js/config.js wmeditor/js

# Compress
# ...

# Build
./encorporate.py src/editor.html wmeditor/editor.html
